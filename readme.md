# OnlineShopTest

## Objectives.

Practice in WebAPI

## Targets.

Simple online shop.

### Description

The website is deployed to Azure.

https://onlineshoptest20181210061039.azurewebsites.net/api/products - returns products in Json.

https://onlineshoptest20181210061039.azurewebsites.net/ - returns simple table with products through JavaScript.

https://onlineshoptest20181210061039.azurewebsites.net/mainpage.html - returns test UI.