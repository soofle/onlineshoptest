﻿using System.Collections.Generic;

namespace OnlineShopTest.Models
{
    public class Cart
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CartProduct> CartProducts { get; set; }

        public Cart()
        {
            CartProducts = new List<CartProduct>();
        }
    }
}