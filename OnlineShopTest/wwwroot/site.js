﻿$(document).ready(function () {
    GetAllProducts();
});

function GetAllProducts() {

    $.ajax({
        url: "/api/products/",
        type: "GET",
        dataType: "json",
        success: function (data) {
            WriteResponse(data);
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function WriteResponse(products) {

    var strResult = "<table><th>ID</th><th>Название продукта</th><th>Цена</th><th>Количество</th>";
    $.each(products,
        function(index, product) {
            strResult += "<tr><td>" +
                product.id +
                "</td><td> " +
                product.name +
                "</td><td>" +
                product.price +
                "</td><td>" +
                product.count;
        });
    strResult += "</table>";
    $("#tableBlock").html(strResult);
}
