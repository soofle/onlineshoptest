﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineShopTest.Models;

namespace OnlineShopTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private static List<Product> _products = new List<Product>()
        {
            new Product {Name = "Vibrator", Price = 10.30m, Quantity = 3},
            new Product {Name = "Dildo", Price = 7.99m, Quantity = 5},
            new Product {Name = "Vagina", Price = 9.99m, Quantity = 7}
        };

        [HttpGet]
        public ActionResult<IReadOnlyCollection<Product>> Get()
        {
            return _products;
        }

        [HttpPost]
        public void AddProduct(Product product)
        {
            _products.Add(product);
        }

        [HttpGet("{id}/purchase")]
        public ActionResult<string> Purchase(int id)
        {
            return Purchase(id, 1);
        }

        [HttpGet("{id}/purchase/{quantity}")]
        public ActionResult Purchase(int id, int quantity)
        {
            var product = _products.SingleOrDefault(x => x.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            if (product.Quantity - quantity >= 0)
            {
                product.Quantity -= quantity;
                return Ok();
            }

            return BadRequest(new ErrorResponse("Not enough products"));
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _products.Remove(_products.Single(x => x.Id == id));
            return Ok();

        }
    }
}
