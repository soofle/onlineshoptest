﻿using Microsoft.EntityFrameworkCore;
using OnlineShopTest.Models;

namespace OnlineShopTest.DataAccessLayer
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Cart> Carts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server = tcp:azureserveronlineshop.database.windows.net, 1433; Initial Catalog = AzureDBOnlineshop; Persist Security Info = False; User ID = SOOF; Password = Azure290792; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasAlternateKey(c => c.Name);

            modelBuilder.Entity<CartProduct>()
                .HasKey(cp => new {cp.CartId, cp.ProductId});

            modelBuilder.Entity<CartProduct>()
                .HasOne(cp => cp.Cart)
                .WithMany(cp => cp.CartProducts)
                .HasForeignKey(cp => cp.CartId);

            modelBuilder.Entity<CartProduct>()
                .HasOne(cp => cp.Product)
                .WithMany(cp => cp.CartProducts)
                .HasForeignKey(cp => cp.ProductId);
        }
    }
}