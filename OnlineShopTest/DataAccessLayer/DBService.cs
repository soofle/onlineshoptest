﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OnlineShopTest.Models;

namespace OnlineShopTest.DataAccessLayer
{
    public class DBService : IDBService
    {
        public DBService(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        private ApplicationContext _dbContext;

        public void Add<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Add(entity);
            _dbContext.SaveChanges();
        }

        public void Remove<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Remove(entity);
            _dbContext.SaveChanges();
        }

        public List<T> FindAll<T>() where T : class
        {
            return _dbContext.Set<T>().ToList();
        }

        public Category FindCategoryWithProducts(string categoryName)
        {
            return _dbContext.Categories.Include(c => c.Products).FirstOrDefault(c => c.Name == categoryName);
        }

        public Product FindProduct(string productName)
        {
            return _dbContext.Products.FirstOrDefault(p => p.Name == productName);
        }

        public Cart FindCartWithProducts(string cartName)
        {
            return _dbContext.Carts.Include(c => c.CartProducts).ThenInclude(cp => cp.Product)
                .FirstOrDefault(c => c.Name == cartName);
        }

        public void PurchaseProduct(int productId, int quantity)
        {
            var product = _dbContext.Products.FirstOrDefault(p => p.Id == productId);
            if (product != null)
            {
                product.Quantity -= quantity;
                _dbContext.SaveChanges();
            }
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}